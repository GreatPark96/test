<?
	class CLS_SESSION_CORE
	{
		var $sid;
		var $item;

		function __construct()
		{
			$this->item = new stdClass();
			if ((string) session_id() === "") {
				$expire = $GLOBALS['conf']['g_expire_session'];
				$session_option = array(
					'gc_maxlifetime' => (($expire > 0) ? $expire : 86400) /* max expire time is 86400 */
				);
				session_start($session_option);
			}
			foreach ($_SESSION as $k=>$v) {
				$this->item->$k = $v;
			}
			$this->sid = session_id();
			return $this->sid;
		}

		function set($p_key, $p_val)
		{
			if (strlen($p_key) < 1) return false;
			$this->debug("set:key:({$p_key}),val({$p_val})");
			$_SESSION[$p_key] = $this->item->$p_key = $p_val;
			return true;
		}

		function get($p_key)
		{
			if (isset($this->item->$p_key))
				return $this->item->$p_key;
			return null;
		}

		function destroy()
		{
			$this->item = new stdClass();
			return @session_destroy();
		}

		function getall()
		{
			$rtn = new stdclass();
			foreach ($this->item as $k=>$v) {
				$rtn->$k = $v;
			}
			return $rtn;
		}

		function reset()
		{
			$r = session_regenerate_id(true);
			$this->sid = session_id();
			return $r;
		}
	}


	class CLS_SESSION extends CLS_SESSION_CORE
	{
		var $expired = null;
		var $debug = false;
		var $log_func = "putlog";

		function __construct($p_debug = false)
		{
			if (isset($p_debug)) $this->debug = $p_debug;

			parent::__construct();
			$old_ip = $this->get('sess_ip');
			$new_ip = get_real_ip();
			if (!empty($old_ip) && $old_ip != $new_ip) {
				$this->destroy();
				$this->reset();
				$this->set('sess_ip', $new_ip);
				write_event(null, ET_Warning, EC_Login, 'login', "Detected Wrong IP: Old[$old_ip] New[$new_ip]");
			}
			else
				$this->set('sess_ip', (empty($old_ip) ? $new_ip : $old_ip));

			/* set expire time */
			$expire = $this->get('sess_expire');
			if (! isset($expire)) {
				$expire = $GLOBALS['conf']['g_expire_session'];
				if (! is_numeric($expire) || $expire < 0) {
					$expire = 1200; // 20 mins
				}
				$this->set("sess_expire", $expire);
			}
			/* set last access time & destory expired session */
			$last_access_time = $this->get("sess_lastaccesstime");
			$this->debug(sprintf("last_access_time(%s),now(%s),expire(%s)", $last_access_time, time(), $expire));
			if ($expire > 0) {
				if (is_numeric($last_access_time) && $last_access_time > 0) {
					if ($last_access_time + $expire < time()) {
						$this->expired = $last_access_time;
						$this->debug("expired");
						$this->destroy();
						return;
					} else {
						$this->expired = null;
					}
				} else {
					$this->expired = null;
				}
			} else {
				// not expire when 0
			}
			$this->set("sess_lastaccesstime", time());
		}

		//코드 호환
		function CLS_SESSION_CORE($p_debug = false)
		{
			__construct($p_debug);
		}

		function putlog($p_msg, $p_level)
		{
			if (function_exists($this->log_func)) {
				$func = $this->log_func;
				return $func($p_msg, $p_level);
			}
			return true;
		}

		function debug($p_msg)
		{
			if ($this->debug) {
				return $this->putlog(" sess_debug: file(".basename($_SERVER['PHP_SELF'])."),".$p_msg, 0);
			}
			return true;
		}

		function getall()
		{
			$all = parent::getall();
			$rtn = new stdclass();
			foreach ($all as $k=>$v) {
				if (substr($k,0,4) == 'sess') {
					$kk = substr($k,5);
					$rtn->$kk = $v;
				} else {
					$rtn->$k = $v;
				}
			}
			return $rtn;
		}
	}
?>
